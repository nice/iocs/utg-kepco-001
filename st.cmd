#!/usr/bin/env iocsh.bash
#
## -----------------------------------------------------------------------------
## EPICS - DataBase
## -----------------------------------------------------------------------------
## Kepco BOP502 Power Supply
## -----------------------------------------------------------------------------
## ESS ERIC - ICS HWI group
## -----------------------------------------------------------------------------
## WP12 - tamas.kerenyi@esss.se
## -----------------------------------------------------------------------------
#
## -----------------------------------------------------------------------------
## EPICS modules being loaded dynamically
## -----------------------------------------------------------------------------
#

require(kepco)

# -----------------------------------------------------------------------------
# Utgard-Lab - enrivonment params
# -----------------------------------------------------------------------------
epicsEnvSet(IPADDR,   "172.30.244.66")
epicsEnvSet(IPPORT,   "5025")
epicsEnvSet(LOCATION, "Utgard; $(IPADDR)")
epicsEnvSet(SYS,      "UTG-SEE-TEFI:")
epicsEnvSet(DEV,      "Tmt-BOP502-02")
epicsEnvSet(PREFIX,   "$(IOCNAME)")
epicsEnvSet(PROTOCOL, "kepco.proto")


# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------

#Utgard base
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")
# Kepco
iocshLoad("$(kepco_DIR)kepco.iocsh", "PREFIX=$(PREFIX), IPADDR=$(IPADDR), IPPORT=$(IPPORT), PROTOCOL=$(PROTOCOL)")

#dbl > "${IOCNAME}_PVs.list"

